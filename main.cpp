//
//  main.cpp
//  Project4
//
//  Rumeysa Bulut
//  150130003
//  Due Date: 23.12.2017
//  Compile using g++ -std=c++11 main.cpp -o <program name>
#include <iostream>
#include <fstream>
#include <sstream>
using namespace std;
class Node{     //Node class. Each node will be inserted to RB tree
private:
    string key;
    char gender;
    int age;
    char colour;
public:
    Node();
    Node *left;
    Node *right;
    Node *parent;
    string getKey();
    char getGender();
    int getAge();
    char getColour();
    void setKey(string);
    void setGender(char);
    void setAge(int);
    void setColour(char);
};
Node::Node(){
    left = NULL;
    right = NULL;
    parent = NULL;
    colour = 'B';
}
void Node::setKey(string name){
    this->key = name;
}
void Node::setGender(char gender){
    this->gender = gender;
}
void Node::setAge(int age){
    this->age = age;
}
void Node::setColour(char colour){
    this->colour = colour;
}
string Node::getKey(){
    return key;
}
char Node::getGender(){
    return gender;
}
int Node::getAge(){
    return age;
}
char Node::getColour(){
    return colour;
}
class Tree{     //Tree class. It has insertion and rotation methods.
public:
    Node *root;
    int counter;
    Tree();
    void rb_insertion(Node *);
    void tree_insertion(Node *);
    void rb_operations(Node *);
    void recursive_insertion(Node *, Node *);
    void left_rotate(Node *);
    void right_rotate(Node *);
    void printInOrder(Node *);
};
Tree::Tree(){   //it creates the tree
    root = NULL;
    counter = 0;
}


void Tree::rb_insertion(Node *newnode){ //newnode to add to the tree
    tree_insertion(newnode);
    newnode->setColour('R');
    rb_operations(newnode);
}
void Tree::rb_operations(Node *nodex){
    Node *uncle;
    if(nodex == root){
        nodex->setColour('B');
    }
    while(nodex != root && nodex->parent->getColour() == 'R'){
        if(nodex->parent == nodex->parent->parent->left){   //parent is the left child of grandparent
            uncle = nodex->parent->parent->right;
            if(uncle != NULL && uncle->getColour() == 'R'){     //uncle's colour is red
                nodex->parent->setColour('B');
                uncle->setColour('B');
                nodex->parent->parent->setColour('R');
                nodex = nodex->parent->parent;      // nodex points to the grandparent now
                rb_operations(nodex);   //new nodex's parent may be red. So operations should be done for new nodex to check colours fix the violation of property 3
            }
            else if(uncle == NULL || uncle->getColour() == 'B' ){       //uncle uncle's color is black
                if(nodex == nodex->parent->left){
                    right_rotate(nodex->parent->parent);
                }
                else if(nodex == nodex->parent->right){
                    left_rotate(nodex->parent);
                    //now nodex is the parent. Both parent and its child are red. It is not allowed. So we need right rotation here.
                    //if nodex does not become root
                    right_rotate(nodex->parent);    //DEBUG, nodex'in neyi gösterdiğine bak.
                }
            }
        }
        //parent is the right child of gp
        else if(nodex->parent == nodex->parent->parent->right){ //Right cases
            uncle = nodex->parent->parent->left;
            if(uncle != NULL && uncle->getColour() == 'R'){
                nodex->parent->setColour('B');  //parent is black
                uncle->setColour('B');      //uncle is black
                nodex->parent->parent->setColour('R');  //grandparent is red
                nodex = nodex->parent->parent;
                rb_operations(nodex);
            }
            else if(uncle == NULL || uncle->getColour() == 'B' ){   //uncle is black
                if(nodex == nodex->parent->left){
                    right_rotate(nodex->parent);
                    left_rotate(nodex->parent);     //DEBUG, nodex'in neyi gösterdiğine iyi bak
                }
                else if(nodex == nodex->parent->right){
                    left_rotate(nodex->parent->parent);     //DEBUG, nodex'in neyi gösterdiğine iyi bak
                }
            }
        }
        root->setColour('B');
    }
}
void Tree::right_rotate(Node *gp){
    char temp;
    Node *gp_left = gp->left;       //gp is the grandparent of nodex, gp_left is the parent of nodex
    gp->left = gp_left->right;      //now, the left of grandparent is the right of parent. we should check if parent's right is NULL or not.
    if(gp_left->right != NULL){
        gp_left->right->parent = gp;
    }
    gp_left->parent = gp->parent;
    if(gp_left->parent == NULL){
        root = gp_left;
    }
    else{
        if(gp == gp->parent->left){
            gp->parent->left = gp_left;
        }
        else{
            gp->parent->right = gp_left;
        }
    }
    gp->parent = gp_left;
    gp_left->right = gp;
    //swapping colours of parent and grandparent
    temp = gp_left->getColour();
    gp_left->setColour(gp->getColour());
    gp->setColour(temp);
}
void Tree::left_rotate(Node *pt){
    Node *pt_right = pt->right;     //pt is the parent and pt_right is the nodex
    pt->right = pt_right->left;
    if(pt_right->left != NULL){
        pt_right->left->parent = pt;
    }
    pt_right->parent = pt->parent;
    if(pt_right->parent == NULL){
        root = pt_right;
    }
    else{
        if(pt == pt->parent->left){
            pt->parent->left = pt_right;
        }
        else{
            pt->parent->right = pt_right;
        }
    }
    pt->parent = pt_right;
    pt_right->left = pt;
    
}
void Tree::tree_insertion(Node *newnode){
    if(root == NULL){
        root = newnode;
    }
    else{
        recursive_insertion(newnode, root);
    }
}
void Tree::recursive_insertion(Node *newnode, Node *leaf){
    if(newnode->getKey() < leaf->getKey()){     // newnode will be added to the right of the leaf
        if(leaf->left != NULL){    //the left node of the leaf contains a node so newnode has to be added to the right or left of the left node of the initial leaf. The function calls itself again
            recursive_insertion(newnode, leaf->left);
        }
        else{
            leaf->left = newnode;
            newnode->parent = leaf;
            //right and left of newnode have already been assigned to NULL.
        }
        
    }
    else{   // newnode->getKey() >= leaf->getKey(), added to the right
        if(leaf->right != NULL){
            recursive_insertion(newnode, leaf->right);
        }
        else{
            leaf->right = newnode;
            newnode->parent = leaf;
        }
        
    }
}

void Tree::printInOrder(Node *node){
    if(node == NULL)
        return;
    counter++;
    printInOrder(node->left);
    for(int i = 0; i < counter*5; i++){
        cout <<"-";
    }
    cout << ">";
    cout << "(" << node->getColour() << ")" << node->getKey() << "-" << node->getGender() << "-" << node->getAge() << endl;
    printInOrder(node->right);
    counter--;
    
}

void readFromFile(const char *filename){    //reads from file and creates nodes. Adds nodes to the tree. Calls insertion method of Tree class
    
    Tree newtree = Tree();
    ifstream inputFile;
    inputFile.open(filename);
    string line;
    string attr;
    if(inputFile){
        while(getline(inputFile, line)){    //after reading each line, it parses the line and sets the value of a node. It creates the nodes.
            Node *newnode = new Node;
            stringstream fullLine(line);
            getline(fullLine, attr, '\t');
            newnode->setKey(attr);
            getline(fullLine, attr, '\t');
            newnode->setGender(attr[0]);
            getline(fullLine, attr, '\t');
            newnode->setAge(stoi(attr));
            newtree.rb_insertion(newnode);
        }
        newtree.printInOrder(newtree.root);
        
    }
    else
        cout<< "Error opening input file" << endl;
    
}

int main(int argc, const char * argv[]) {
    readFromFile(argv[1]);
    return 0;
}
